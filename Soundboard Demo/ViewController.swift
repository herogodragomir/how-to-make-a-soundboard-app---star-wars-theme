//
//  ViewController.swift
//  Soundboard Demo
//
//  Created by Edy Cu Tjong on 6/7/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    let soundFilenames = [ "light-saber-on", "blaster-firing", "chewy_roar", "swvader02" ]
    var audioPlayers = [AVAudioPlayer]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up audio players
        for sound in soundFilenames {
            do {
                if let path = Bundle.main.path(forResource: sound, ofType: "wav") {
                    let url = URL(fileURLWithPath: path)
                    let audioPlayer = try AVAudioPlayer(contentsOf: url)
                    audioPlayers.append(audioPlayer)
                } else {
                    audioPlayers.append(AVAudioPlayer())
                }
            } catch {
                audioPlayers.append(AVAudioPlayer())
            }
        }
    }

    @IBAction func buttonTapped(_ sender: UIButton) {
        guard audioPlayers.indices.contains(sender.tag) else { return }
        let audioPlayer = audioPlayers[sender.tag]
        audioPlayer.play()
    }
}

